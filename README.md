# bradphipps.gitlab.io

El Blog - https://bradphipps.gitlab.io/

# Development

Install prerequisites:
```
$ python3 -m venv venv
$ . venv/bin/activate
$ pip install -r requirements # run twice if the blinker+feedgenerator wheel warning red text scares you
```

Run local webserver and autoreload on changes:
```
$ make regenerate
# Open new terminal
$ make serve
```
