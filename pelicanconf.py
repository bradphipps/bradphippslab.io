#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'brad'
SITENAME = 'Brad Phipps'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'output'

TIMEZONE = 'Europe/London'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Get Pelican', 'http://getpelican.com/'),
#        ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (
             ('Gitlab', 'https://gitlab.com/bradphipps'),
             ('Github', 'https://github.com/bradphipps'),
             ('LinkedIn', 'https://www.linkedin.com/in/brad☕/'),
         )

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# ref - http://docs.getpelican.com/en/stable/content.html#publishing-drafts
DEFAULT_METADATA = {
    'status': 'draft',
}

# ref - https://github.com/getpelican/pelican/wiki/Tips-n-Tricks#second-solution-using-static_paths
STATIC_PATHS = ['images', 'extra']
EXTRA_PATH_METADATA = {
                          'extra/favicon.ico': {'path': 'favicon.ico'},
                      }

# ref - http://docs.getpelican.com/en/stable/themes.html#creating-themes
# THEME = 'simple'

# ref - http://docs.getpelican.com/en/stable/settings.html?highlight=default_date#time-and-date
# DEFAULT_DATE = 'fs'
