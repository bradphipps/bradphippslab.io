title: Setting up a free blog using Pelican and Gitlab Pages
date: 2018-12-30
category: Blog
status: published
tags: pelican, python, ci

# Requirements

* Markdown syntax for distraction free writing.
* No need to learn a new programming language just to write a blog (No Hugo or Next.js for me, yet).
* Simple workflow: write something in Markdown, git push, see result a few mins later.
* Make use of Gitlab/Github pages to host for free.

# Pick up a Pelican

Technology selection process:

1. Go to Google
1. Search for 'static site generators'
1. Click first result - `staticgen.com`
1. Filter generators:
    * Language: `Python` (I've done Python before)
    * Template engine: `Jinja2` (I've done Ansible templates before)
1. Choose Pelican based on that it fits above filter and host most GitHub stars.
1. Double check project is still active by viewing last few releases CHANGELOG entries.
1. Filter issues with most 👎 and comments to see if project has any major flaws.
1. LGTM ✅

# Other generators I tried

_Ain't nobody got time for that._

# Setup

1. Follow [official Pelican docs](http://docs.getpelican.com/en/stable/quickstart.html).
1. Follow [official Gitlab Pages docs](https://docs.gitlab.com/ee/ci/yaml/#pages).
1. That's about it. Hurray for good docs.

# First issues

[Simple theme is broken on pelican 4.0.1](https://github.com/getpelican/pelican/issues/2489) so can't extend it to make my own custom theme, yet.
